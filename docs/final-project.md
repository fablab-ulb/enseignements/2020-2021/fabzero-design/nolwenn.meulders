# Projet final 

Sognare est une veilleuse de nuit qui à pour but d'aider les plus petits à entrer dans le pays des rêves. L'objet participe au rituel du coucher de l'enfant en lui permettant de, chaque soir, définir les couleurs qui le berçeront. Le nom _Sognare_ signifie "rêve" en italien. Il est un clin d'oeil à l'ojbet de référence, **la chaise d'enfant Chica**, dont les designers sont italiens. 

![](images/new.png)

Les professionnels du sommeil et de l'enfance en sont certains, le rituel du coucher est essentiel pour permettre à l'enfant de s'endormir paisiblement. Plusieurs méthodes sont préconisées pour amener l’enfant à apprécier le fait d’aller dormir.   La veilleuse pour enfant, Sognare, vise à participer à ce rituel en le rendant plus ludique.   
L'objet est pensé simplement. C'est une boîte lumineuse dans laquelle on vient déposer différents cubes de couleurs en fonction de ses humeurs. Ces cubes fonctionnent de manière autonome, comme des pièces de légo. Ils sont principalement utilisés pour décider des couleurs que la veilleuse diffusera, mais aussi en tant que jeux de construction.   
À noter l'importance des couleurs proposées dans ce projet. Chacune a sa propre signification, correspond à une émotion. Les couleurs chaudes comme le rouge et l'orange ont par exemple tendance à rassurer l'enfant. Les couleurs plus froides comme le bleu et le vert ont des vertus plus apaisantes, relaxantes.   
Les repères temporels sont également importants dans le rituel et permettent de diminuer l'anxiété. Chaque soir, les parents peuvent demander ce que ressent l'enfant, s'il a aimé sa journée et qu'est ce qu'il a envie de faire le lendemain. Il peut alors exprimer ses envies et ses craintes à travers la création de sa propre lumière colorée. 

**Si vous voulez en savoir plus sur les différents usages de l'objet au cours du temps ainsi que sur son catalogue de couleurs, vous pouvez aller jeter un coup d'oeil dans la section _Recherches Sognare_**.

## Fichiers du projet

| Boîte lumineuse | Pièce carrée 1mm | Pièce carrée 0,6mm | Pièce rectangle 1mm | Pièce rectangle 0,6mm |
| ------ | ------ | ------ | ------ | ------ |
| [Télécharger](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/nolwenn.meulders/-/raw/master/docs/images/boite_lumineuse_.stl?inline=false) | [Télécharger](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/nolwenn.meulders/-/raw/master/docs/images/Pièce_carré_1mm.stl?inline=false)| [Télécharger](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/nolwenn.meulders/-/raw/master/docs/images/Pièce_carré_0_6_mm.stl?inline=false) | [Télécharger](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/nolwenn.meulders/-/raw/master/docs/images/Pièce_carré_0_6_mm.stl?inline=false) | [Télécharger](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/nolwenn.meulders/-/raw/master/docs/images/Pièce_longue_0_6mm.stl?inline=false) | 

Ce projet contient différentes pièces
-  La boite lumineuse : **15x15x6 cm**   
-  Pièce carrée pour les impressions de couleurs chaudes : **5x5x4 cm et filtre de 1mm d'épaisseur**  
-  Pièce carrée pour les impressions de couleurs froides : **5x5x4 cm et filtre de 0,6mm d'épaisseur**  
-  Pièce rectangle pour les impressions de couleurs chaudes : **10x5x4 cm et filtre de 1mm d'épaisseur** 
-  Pièce rectangle pour les impressions de couleurs droides : **10x5x4 cm et filtre de 0,6mm d'épaisseur**

# Recherches et évolution

Avant de commencer la lecture de mon projet, il est nécessaire de rappeler l'objet que j'ai choisi d'étudier lors de ce quadrimestre. Il s'agit de **_la chaise d'enfant, Chica_** du collectif de desinger _BBB Bonacina_. 
J'ai décidé de la choisir pour son aspect modulable, combinable. C'est une chaise aux couleurs vives, et forme simple. La chaise est à la fois une assise, mais également un jeu pour les enfants. 

[](images/Chaises_d_enfants.jpg)  

## Attitude face au projet
Pour la conception de notre projet, on doit choisir un mot qui sera vecteur de notre processus de pensée. On a à notre disposition une liste de 5 mots. On peut trouver des nuances en chacun d'eux en fonction du dictionnaire utilisé. Par exemple  le **Wikictionnaire** est un dictionnaire contemporain, ses définitions évoluent sans cesse. **Le Littré** est quand un lui un dictionnaire plutôt littéraire, qui date du 19ième siècles.  Les 5 mots proposés sont les suivants :   
• Référence  
• Influence   
• **Inspiration**  
• Hommage  
• Extension  

Pour la poursuite de mon projet, j'ai décidé de choisir comme mot guide le mode **Inspiration**. 

* **Analyse des définitions du mot "Inspiration"**   

Malgré certaines nuances, on peut observer que chaque dictionnaire donne un sens commun au mot inspiration. 
L'inspiration peut signifier: 
1. la phase de la respiration pendant laquelle l'air atmosphérique, riche en oxygène, pénètre dans les poumons
2. L'enthousiasme qui entraîne les poëtes, les musiciens, les peintres. Exmple; ce vers a été écrit d'inspiration
3. Une action de conseiller quelqu'un, de lui suggérer quelque chose
4. **L'influence exercée sur un auteur, sur une œuvre**
5. l'action d'inspirer, de conseiller qqch. à qqn ; résultat de cette action. ➙ influence, instigation.

Mais une nuance du dictionnaire **Wiktionnary** m'a particulièrement intriguée. Selon lui, l'inspiration signifierait aussi **_un acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence._**


* **Pourquoi ce choix ?**   

La définition du Wiktionnary a éveillé ma curiosité. J'ai donc décidé de choisir le mot inspiration comme ligne de conduite. Mon projet référant est **la chaise d'enfant** de _BBB Bonacina_. Pour mon projet final, je m'inspire des concepts de la chaise d'enfant. C'est-à-dire, ses concepts de modularité, sa possibilité de combinaisons infinies,... J'aimerais appliquer ces principes simples de manières plus larges à mon projet. La première idée est de créer une chaise entièrement modulable et combinable, comme une construction en légo. En plus de m'inspirer des principes logiques de la chaise, je m'inspire de son utilité en dédiant mon objet final pour les enfants. Mon projet serait à la fois une chaise mais, aussi un jeu. Une chaise que les enfants pourraient construire eux-mêmes. 
L'inspiration c'est pour moi le début de toute création, c'est le fait d'être mentalement stimulé à créer quelque chose, à avoir des idées. La chaise de _BBB Bonacina_ m'a donné envie d'aller plus loin dans la réflexion du concept de combinaison, de modularité, d'emboitement et de jeux pour enfants.  

* **Inspiration de la chaise**

Avant de commencer le projet final, j'ai écrit tout ce qui m'inspirait de la chaise d'enfant. Soit les concepts que j'ai envie de mettre en place dans le projet final. Ce qui m'inspire dans le projet référent est : 

```La modularité```  
```L'assemblage des pièces```  
```Les différentes combinaisons possible```  
```Un desing destiné à l'enfance qui associce l'utile et le ludique```  

### Première idée 

**Utiliser le principe du légo**   
Selon la définition du dictionnaire Larousse, le légo c'est _ un jeu de construction constitué de pièces de plastique dur qui s'encastrent les unes dans les autres_. J'ai donc imaginé plusieurs petites pièces qui s'assemblent pour former une chaise, comme un jeu de légo. Des petites pièces emboitables, aux couleurs vivent qui s'impriment grâce à l'imprimante 3D. 

![](images/pièce.jpg)

L'intérêt de cette conception, c'est qu'elle est entièrement constructible par un enfant. On peut modifier son apparence avec des pièces de différentes couleurs. C'est un jeu qui mêle le ludique au fonctionnel. 

![](images/Chaise.gif) ![](images/chaise_final.jpg)

**Problèmes découvert lors de la phase test**  
En présentant mon prototype lors d'une correction, je me suis rapidement rendu compte que ce projet était difficilement réalisable pour plusieurs raisons. 
1. Pour une question de stabilité, une chaise en légo est trop dangereuse et peu stable pour un jeune enfant.
2. Imprimer une chaise d'enfant à l'échelle 1:1, avec l'imprimante 3D, prendrait beaucoup trop de temps. De plus, l'assise et le dossier sont trop grands pour être imprimer. Il faudrait donc utiliser d'autre machines pour la réalisation de ces éléments.    

Après quelques réflexions, j'ai donc décidé de changer d'idée. 


### Deuxième idée

Pour cette deuxième idée, j'ai d'abord observé ce qu'il y avait dans une chambre d'enfant. J'ai tout de suite pensé à une étagère qui pourrait évoluer dans le temps en fonction des besoins de celui-ci.  
Contrairement à la chaise en légo dessiné plus haut, l'étagère ne serait pas un jeu ludique mais seulement un mobilier de petite taille destiné à une chambre d'enfant. 
L'idée est de créer une étagère évolutive dont qu'on pourrait facilement ranger quand on n'utilise pas certains modules (les modules s'auto-construisent avec des plaques de bois multi-plex). Les plaques de bois sont détachables facilement et peuvent toutes s'entreposer en prenant le moins de place possible. 

Pour cette création je me suis également inspireé du groupe de designer GAP. Ainsi selon eux ; 
    "un objet bien designé est un objet auto-démonstratif, un objet qui dit lui-même son concept". 

**Particularité de l'étagère**

* L'étagère cible les enfants de 6 mois (âge des premiers pas) à 10 ans. 
* La hauteur de chaque module doit être adaptée aux enfants et à leur taille. 
* Le charge maximum par module d'étagère est de 9kg

**Relfexion sur l'emboitement des modules** 

![](images/1.jpg) ![](images/Encastrement.gif)

![](images/4.jpg) ![](images/6.gif)

**Recherche de forme, première esquisse**

Comme première esquisse, j'ai tout d'abord imaginé des modules carrés ainsi qu'une nouvelle sorte d'emboîtement. 

![](images/shéma.jpg)

Avec ensuite un premier test en maquette. 

![](images/Photo_maquette.jpg)

Pour cette maquette, j'ai dû utiliser deux patrons différents afin que l'emboîtage puisse se faire facilement dans un seul sens.

![](images/patron.jpg)

J'ai ensuite essayé les différentes formes qu'on pouvait réaliser grâce aux modules. Les possibilités d'évolution de l'étagèreé sont variées. 

![](images/docs_images_maquette_étagère.jpg)    

© Maquette en carton gris, imprimée au laser


## Idée finale 

**Remise en question**

Je m'égare,.... Je m'éloigne de mon projet d'inspiration. Je sens bien que l'étagère ne me plaît pas. J'ai choisi la chaise de BBB Bonacina parce que c'est pour moi un objet plein de vie, avec lequel on a envie de jouer. C'est un objet super-personnel, à travers lequel on ressent les envies et la personnalité des designers. Soi, tout ce que je ne fais pas. 

Il faut que je me resitue et que je remette sur papier exactement les concepts que je veux appliquer dans mon projet. 
C'est a dire :
- **Avoir des couleurs vives, qui rendent mon objet attractif et ludique visuellement** 
- **Des pièces finies et unies qui s'emboitent**
- **Un objet du quotidien qui est à la fois utile et récréatif**

**Nouvelle idées, nouvel objet**

Après une discussion avec Max et Margaux sur, qu'est-ce qu'aimait un enfant, je me suis vite penchée sur l'idée d'une veilleuse, le petit objet indispensable de l'enfance.   
Il m'est alors venu l'idée de créer une veilleuse récréative et ludique, comme mon objet d'inspiration. 
Différentes pièces éclairées qui, associées ensemble, créent des ambiances lumineuses variées. 
En plus d'être associable, ces pièces pourraient s'emboîter les unes dans les autres, tout comme la chaise de BBB Bonacina. 

![](images/animaux_copie.jpg)
© Dessin d'esquisse, recherche de projet

**Modélisation de l'objet**

Pour mon idée de projet, j'ai besoin de modéliser ;
- Une boîte qui servira de support pour la lampe 
- De pièces légo 
- Une fine couche de plastique pour les filtres translucides. Celle-ci doit être très fine afin de laisser passer la lumière

**Attention**, je dois bien vérifier la dimension de toutes mes pièces afin que celles-ci s'emboîtent correctement. 

![](images/piece_1.jpg)

**Résultat**

![](images/Maquette.jpg)

![](images/Explications_A4.jpg)

# Recherches Sognare 

## Une veilleuse, qu'est-ce-que c'est ? 

Afin de mieux concevoir mon objet, je dois tout d'abord le comprendre. Après plusieurs recherches, j'ai pu lister les différents usages qu'une veilleuse pouvait avoir au cours de sa vie.
1. Quand l'enfant n'est encore qu'un nourrisson (moins de 18 mois), la veilleuse sert aux parents. Elle leur permet de s'occuper du bébé la nuit sans l'éblouir avec une trop grosse lumière. Cela permet de ne pas perturber son sommeil. 
2. Une fois l'enfant plus grand, les premières peurs du noir apparaissent. La veilleuse a pour rôle d'accompagner leur sommeil et de les rassurer. 
3. Elle peut-être également utilisée comme lumière d'ambiance 
4. Les veilleuses de nuit servent aussi de guide dans le noir. En indiquant par exemple le chemin de toilettes. 

Pour ce projet, j'ai décidé de m'instéresser plus particulièrement au deuxième aspect énonce ci-dessus. Soit, comment faire en sorte que mon objet ai un impact positif sur le sommeil des enfants.  

## Le rituel du coucher 

Les professionnels de l'enfance et du sommeil en sont certains, le rituel du coucher est essentiel pour permettre à l'enfant de s'endormir paisiblement. Pour avoir un impact positif sur le sommeil il faut donc, sans nul doute, s'intéresser plus amplement à ces petits rituels. Ceux-ci peuvent être divers (brossage de dents, lecture d'une petite histoire, câlin avant de dormir,...), mais doivent fonctionner de manière répétée chaque soir. Après plusieurs recherches, j'ai pu dégager quelques petits actes à préconiser pour avoir l'impact le plus positif possible (ceci est une liste non-exhaustive, il en existe beaucoup d'autres). 
* Rendre ce rituel ludique permet de diminuer son appréhension au fait d'aller dormir. 
* Les repères temporels sont également importants dans le rituel et permettent de diminuer l'anxiété. Un repère temporel, c'est lui donner la possibilité de parler de sa journée, de ses ressentis. C'est lui demander comment il appréhende la journée du lendemain,... Toutes ces petites questions le rassurent, il finira ainsi sa journée plus sereinement. 
* Ces moments doivent être calmes et paisibles, il faut créer une atmosphère propice au sommeil. L'idéal, c'est de fermer les rideaux/volets. Il faut éviter les lumières trop fortes qui l'excitent. 

J'ai tout de suite trouvé intéressant d'appliquer ces différents concepts au projet de la veilleuse. 

## L'impact des couleurs 

La chromothérapie est une médecine non-conventionnelle inspirée des traditions indienne et chinoise qui prône la guérison de certains trouble par l'utilisation de la couleur. Les couleurs provoquent des stimulus sensorielles qui s'active dans notre rétine. C'est la raison pour laquelle couleur et émotions sont souvent associées.  
Chaque couleur faisant partie du spectre basique offre des bienfaits spécifique, chacune à sa propre signification. Il est intéressant d'en prendre connaissance afin d'en tirer partit dans mon projet de veilleuse.   
On peut diviser le spectre des couleurs en deux. Les couleurs chaudes et les couleurs froides. Les couleurs chaudes sont plus stimulantes, rassurantes. Les couleurs froides ont quant-à elle des vertus plus apaisante et régulatrice. 

![](images/Couleur.png)

## Petite histoire de vie. 

Il est 19h, l'heure pour la petite Laura d'aller se coucher. Depuis quelques semaines, elle n'appréhende plus ce moment, et n'a plus peur de se retrouver seule dans sa chambre. C'est grâce au nouveau rituel du coucher, que ses parents ont récemment instauré.
A 19h30 Laura a fini de se brosser les dents et court dans sa chambre pour utiliser sa nouvelle lumière fétiche, la veilleuse Sognare. Après une discussion avec ses parents sur le déroulement de sa journée et ses émotions, Laura décide d'insérer des cubes de couleurs bleues et vertes dans sa veilleuse. En effet, elle se sent un peu excitée et aimerait être plus relaxée pour bien dormir cette nuit. Cependant, même si ca fait longtemps que ce n'est plus arrivé, elle n'a pas envie d'avoir peur du noir. Elle décide donc de quand même mettre un petit cube rouge dans sa veilleuse, pour lui donner du courage. 

![](images/Hnet-image-2.gif) 

## Tests et évolution du modèle

Le modèle se présente comme suit ; une boite carrée dans là qu'elle vient s'insérer des pièces differentes. Dans cette boite, se trouvent les petites lumières LED, pour éclairer de manière homogène toutes les pièces.   
La particularité du projet, c'est que les pièces doivent s'emboîter les unes dans les autres. Car elles sont aussi comme un jeu de construction 

![](docs/images/croquis.png)
 
### Modélisation sur fusion 

**Modélisation de la boîte**

<iframe src="https://myulb124.autodesk360.com/shares/public/SH919a0QTf3c32634dcfee0c13f702b4a19d?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

![](images/boîte.png)

**Modélisation de la plaque pour accrocher les ampoules**

<iframe src="https://myulb124.autodesk360.com/shares/public/SH919a0QTf3c32634dcf2c22cbbd0920e806?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

**Les différentes pièces**   

Suite aux premiers essaies du pré-jury, je me suis rendu compte que les couleurs chaudes avaient tendance à être beaucoup moins opaque que les froides. J'ai alors imprimer différents filtres pour tester leurs opacités. J'en ai déduit une épaisseur optimale en fonction de chaque couleur afin d'optimiser le rendu. Pour chaque pièce, j'ai dessiné deux modèles ; **un avec un filtre de 1mm d'épaisseur pour le rouge, l'orange et le jaune et l'autre avec un filtre de 0,6mm d'épaisseur, pour le bleu, le turquoise et le vert.** 
J'ai aussi mis des attaches au-dessus de chaque pièce, pour permettre leurs emboîtements.

| Largeur | Longeur | Hauteur | Epaisseur de la coque | Epaisseurs attaches | Eloignement des attaches du bord |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 5cm | 5cm | 4cm | 4mm | 1mm | 4mm |

J'ai tout d'abord tracé l'esquisse de ma boite pour ensuite l'extrudé. J'ai ensuite tracé le filtre que j'ai extrudé en fonction de l'épaisseur que je voulais avoir (0,6 ou 1mm). Pour finir, j'ai dessiné tout au-dessus les attaches des pièces. 
![](images/cube_1_.png)

Après les premiers tests d'impression, je me suis rendu compte que les attaches étaient beaucoup trop fines, et donc fragiles. Il a fallu augmenter leurs épaisseurs pour les solidifier. 
En augmentant leurs épaisseurs, l'emboîtement des pièces entre elles ne se faisait plus correctement. J'ai donc du faire plusieurs essaies pour comprendre à quelle distance je devais mettre les attaches du bord pour que les différentes parties puissent s'emboîter, sans qu'elles restent coincées. Pour modifier les mesures, j'utilisais l'outil *modifier les paramètres**. Il permet de changer toutes les distances sans devoir recommencer le modèle. Voici les paramètres finaux de la modélisation : 

| Largeur | Longeur | Hauteur | Epaisseur de la coque| Epaisseurs attaches | Eloignement des attaches du bord |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 5cm | 5cm | 4cm | 4mm | 2mm | 4,25mm |

Une fois les pièces cubiques correctement modélisées, **j'ai appliqué le même principe à des pièces rectangulaires**. 

![](images/jaune.png)  

## Impression 3D

**Impression des pièces**
1. L'impression 3D à été beaucoup plus compliqué à paramétrer. J'ai d'abord tenté d'imprimer avec les paramètres suivants. 

![](images/PREMIER_PARAM7TRE.png)

Le filtre étant beaucoup trop fin, il se cassait quand j'enlevais les supports. 

2. J'ai alors tenté une impression sans supports pour éviter que le filtre ne se casse (échec, la partie supérieure gondole).

![](images/raté.png)

3. J'ai ensuite changé les paramètres d'espacement des supports, pour qu'ils soient plus facilement retirables : 
- **Espacement des motifs** : **5 mm**
- **Couches d'interface** : **5 couches**
- **Espacement du motif d'interface** : **5 mm**

Le résultat est beaucoup plus concluant et permet au filtre de s'imprimer correctement, sans se casser à cause des supports. Cependant, le rendu reste trop opaque. La lumière à du mal à se diffuser à travers la couche supérieur des blocs. Et si je modélise des filtres plus fins sur fusion, ils ne s'impriment pas. 

4. Pour remédier à ce problème, il faut jouer avec le nombre de couches solides horizontales. Lors des premières impressions, les paramètres étaient définis sur **3 couches**. Il faut le **changer et en mettre 2**. Voici donc les paramètres finaux à encoder pour imprimer correctement les pièces sur PrusaSlicer.  

![](docs/images/para_finaux.png)

![](images/tour.png)

### Photos du projet final

![](images/d.png)




