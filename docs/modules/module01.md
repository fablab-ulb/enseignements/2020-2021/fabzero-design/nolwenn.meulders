# Git

## Installation et identifiation

La première chose à faire avant de commencer à utiliser le programme GitLab, était d'installer Git sur mon Mac et de le configurer. J'ai donc suivi le tuto de configuration disponible sur ce [lien](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git).  
La première commande à effectué était <git --version>. Mais mon ordinateur n'avait pas de ligne de commande et ne pouvait pas exécuter ma demande.

![photo1](../images/Photo1.png)

J'ai alors décidé d'installer une ligne de commande Xcode disponible sur ce [lien](https://developer.apple.com/download/more/).  
J'ai ensuite pousuivis la configuration de Git en entrant mes informations d'identification.

![photo2](../images/Photo_2.png)

La prochaine étape consistait à l'authentification via une clé SSH. Grâce à cette clé, je peux travailler localement sur mon ordinateur, la communication entre le Git et son serveur est alors sécurisée.
La clé SSH permet aussi une authentification automatique. Je ne devrai plus entrer mon nom d'utilisateur, mot de passe,...  
Pour créer une clé SSH, j'ai suivi ce [tuto](https://docs.gitlab.com/ee/ssh/README.html)

![photo3](../images/Photo_3.png)

Une fois ma clé SSH généré et ma "phrase de passe" crée, j'ai pu voir l'emplacement de ma clé de commande.
J'ai donc pu copier ma clé publique générer sur Gitlab. Seulement, mon fichier ne s'ouvrait pas.  
La solution trouvée était de télécharger Atom, et d'ouvrir mon fichier (.pub) avec ce programme.

![photo4](../images/Photo_4.png)

L'installation est enfin terminée.




## Prise en main de Gitlab

Je n'ai pas pu accéder au cours du jeudi 8 octobre 2020, qui expliquait la prise en main et le fonctionnement de GitLab.
J'ai dû alors apprendre à manipuler et comprendre ce qu'il fallait remettre par moi-même.

Pour cela le site est très bien conçu car on à accès au travail des autres étudiants. Par le billais de leur profil j'ai pu comprendre ce qu'il fallait rendre, et qu'elles étaient les manipulations qu'ils avaient effectuées pour atteindre un résultat (comment mettre une photo, ou placer notre présentation, comment mettre un titre)? Toutes ces choses qui paraissent "simples", mais que ne sont pas innées.
Si le rendu d'un élève vous plaît et que vous désirez voir ses manipulations d'encodage vous devez cliquer sur ce bouton une fois le fichier ouvert.  

![](../images/commande.jpg)

Après cette première prise en main, j'ai cherché à comprendre plus en profondeur le fonctionnement du programme. J'ai donc lu attentivement ce [tutoriel](https://gitlab.com/jnaour/tutoriel-gitlab/-/wikis/tutoriel-gitlab) qui explique vraiment bien l'utilisation de GitLab aux débutants, comme moi.

Une fois la prise en main faite, j'ai remarqué que toutes les modifications effectuées à mon travail ne s'effectuait pas à mon dossier via le groupe du Fablab.  
J'avais en effet crée un autre projet sans le savoir (un [fork](https://www.christopheducamp.com/2013/12/16/forker-un-repo-github/)), qui ne correspondait pas au projet sur le quel je devais travailler initialement.

"Un fork est une copie d’un dépôt. Forker un dépôt vous permet d’expérimenter librement des modifications sans toucher au projet original".
En d'autres thermes, on utilise un fork pour effectuer des modifications sur un travail existant, si on veut utiliser ce travail comme base de travail,...  
Le créateur du projet initial peut alors utiliser et avoir accès à votre fork (l'autre version du travail modifié).










