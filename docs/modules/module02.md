# Modélisation de l'objet sur fusion

## Expérimentation, premier jet

Pour ce module, j'ai dû redessiner mon objet (la chaise d'enfant) sur le programme fusion 360.
Ma première initiative était de créer un modèle simplifié de mon objet afin de mieux comprendre son fonctionnement, ses capacités ainsi que le programme Fusion.  La version simplifiée ne devait être composée que de l'assise, du dossier, de quatre pieds inférieurs fins et deux pieds supérieurs plus épais et creux.

* Pour commencer mon modèle d'expérimentation, j'ai d'abord créé l'assise de ma chaise. J'ai ensuite dessiné des cercles sur cette assise que j'ai extrudés pour en faire les pieds. 
Cependant, une fois cette structure faite je n'arrivais pas à attacher le dossier aux pieds. La forme cylindrique ne me permettait pas d'y fixer un plan d'esquisse.  
Pour faciliter la modélisation, j'ai décidé d'inverser le processus et de créer d'abord mon esquisse de dossier, avant d'y ajouter les pieds. Les pieds une fois extrudés, fusionnent avec le dossier. Je peux alors effectuer la commande "perçage" dans le pied supérieur afin de créer un pied creux (permet d'assembler les chaises ensemble).

![Photo 6 et 7,8 ](../images/photo_6-7-8.png)

* Pour supprimer le dossier qui débordait des pieds j'ai utilisé la commande "scinder les corps", pour ensuite masquer les corps qui dépassaient de mon objet. 

![Photo 9 et 10 ](../images/photo_9-10.png)

* Mon modèle presque terminé, il ne me restait plus qu'a "courbé" la bout de l'assise. J'ai alors tracé l'esquisse de la courbure que je souhaitais avoir. L'outil "balayage", m'a permis de faire suivre mon plan le long de la trajectoire donnée (la courbure).
Une fois fini, la courbure dépassait légèrement du plan. J'ai donc à nouveau utilisé l'outil "scinder les corps" pour supprimer la partie qui dépassait de mon objet.
Ma petite chaise est enfin terminée. 

![photo 11, 12 et 13](../images/photo_11-12-12.png)

**Problèmes détectés**   
Une fois ma chaise imprimée, je me suis rendu compte de différents problèmes (CF module 3).
 1. Les perçages dans les pieds supérieurs de ma chaise étaient légerment trop large  
 2. Les pieds supérieurs et inférieurs n'étant pas parfaitement alignés, l'empilement des chaises était impossible  



## Modification après l'expérimentation

### Analyse plus approfondie de la chaise
Pour pouvoir réaliser un modèle le plus fidèle possible, il a fallu que j'analyse plus profondément l'objet choisi.
* Contrairement à mon modèle d'expérimentation, les quatre pieds ainsi que la structure du dossier sont de formes et de tailles identiques. Ils sont comme un cône creux, permettant l'emboitement des pièces les unes dans les autres. Sur ma chaise créée précédemment, les pieds inférieurs de l'assise étaient minces afin de pouvoir rentrer dans la structure du dossier, plus épaisse. Dans le modèle original, comme les pieds inférieurs et ceux de la structure du dossier ont la même épaisseur, les pieds avant de l'assise peuvent eux aussi accueillir d'autres pieds.  
* Il sagit d'une chaise pour enfants. La chaise est beaucoup moins "élancée" que ce que j'ai dessiné précédemment. Les pieds sont plus petits et plus gros. 
* La taille de la chaise dépend d'un modèle à l'autre, mais fait en moyenne 50cm de hauteur pour une assise de 30x30cm. 

### Modélisation 

Pour cette modélisation, j'ai remarqué un très net changement quant à l'utilisation du programme. Je l'utilisais de manière beaucoup plus fluide et j'ai exploré un peu plus tout ce que celui-ci pouvait offrir. 
Pour commencé j'ai été voir un [tuto](https://www.youtube.com/watch?v=pDECKXFHmOQ ) fusion sur YouTube, qui expliquait rapidement l'utilisation des composants. Quand on crée un nouveau composant, tout ce qu'on dessine s'enregistre dedans. On peut ensuite le masquer, le modifier, assembler les composants ensembles, l'isoler,... En bref, pour une bonne utilisation de fusion il est impératif de maitriser cette technique. Il faut savoir que tout mon projet a été dessiné à l'échelle 1/10ième. 
* La première chose que j'ai déssinée, c'était mon pied creux, de forme conique. J'ai créé une première esquisse circulaire, qui serait le contour inférieur du pied. J'ai ensuite cliqué sur **"construire"** puis **"plan de décalage"**. Cela m'a permis de créer un plan d'esquisse 35mm plus haut (plan 1), et de dessiner le contour supérieur de mon pied.(**1**).   
Une fois ces deux premières esquisses réalisées, j'ai appuyé sur **"créer"** puis **"lissage"** pour créer la forme conique.(**2**)  
Pour creuser la forme, il faut utiliser l'outil **"coque"**, et définir l'épaisseur que celle-ci doit avoir. Dans notre cas, 1mm. 
L'outil coque, contrairement à l'outil perçage, permet de creuser une cavité qui suit l'enveloppe de l'objet et son changement d'épaisseur (ici le trou est plus épais au-dessus qu'en bas).(**3**)

![](../images/photo_20.jpg)

* Ensuite j'ai voulu vérifier l'assemblage des pieds. J'ai donc dupliqué le composant actuel pour en créer une autre avec le même corps d'objet à l'intérieur. J'ai déplacé mes objets pour qu'ils se positionnent l'un sur l'autre. En cliquant sur **"Inspecter"** puis **"Analyse de section"** on peut effectuer une coupe, selon un plan défini, dans les objets. Ce paramètre m'a permis de comprendre comment s'assemblaient mes pieds et de voir si ça fonctionnait.(**1**)  
En voyant l'emboitement de mes deux pièces, j'ai cherché à modifier l'épaisseur de mes pieds pour que l'assemblage soit le plus efficace possible. Pour changer un paramètre du dessin sans devoir tout recommencer, il y a une commande très efficace qui s'appelle **"modifier les paramètres"**. C'est en fait un tableau excel qui reprend toutes les mesures du dessin. J'ai pu modifier l'épaisseur de mon pied en voyant directement l'impact des modifications en coupe.(**2**)  
Après plusieurs essaies, j'ai remarqué que les paramètres idéals pour un bon assemblage étaient : 
    * un diamètre de 4mm pour la partie inférieure du pied 
    * Un diamètre de 8mm pour la partie supérieure du pied
    * Une épaisseur de 8mm pour la coque 
    * Une hauteur de 27mm  
Pour réduire la taille de mes pieds, j'ai simplement dû insérer une nouvelle mesure pour **le plan 1** (ici 27mm au lieu de 35). Ce plan 1 correspond au plan dans lequel l'esquisse de mon pied supérieur a été dessiné.(**3**)


![](../images/Photo_21.jpg) 

* Une fois que mes pieds et ma structure étaient réalisés, j'ai créé (toujours dans un nouveau composant) mon assise. Pour cette partie, j'ai reproduit exactement les mêmes manipulations que pour ma première modélisation. C'est-à-dire **"esquisse"** et **"extrusion"** pour créer le volume carré et enfin **"balayage"** pour former la courbure. 

* Pour placer les pieds exactement au bon endroit sur mon assise, j'ai dessiné un carré sur celle-ci. Ensuite, il me suffisait simplement d'utiliser la commande **"déplacer"**, sélectionner le composant à déplacer puis d'utiliser l'outil **"de point à point"**. Grâce à cet outil, le point d'origine (ici le point central des faces de mon cône), se déplacent automatiquement sur le point cible sélectionné (un des côtés du carré dessiné). Grâce à cette commande, je suis sur que mes pieds sont, cette fois, parfaitement alignés et à la bonne place. 

![](../images/photo_22.jpg)

* Pour que les trous des pieds avant de la chaise apparaissent sur l'assise, j'ai dû extrudé un cercle ayant le même diamètre que le pied supérieur.

* Pour faire le dossier, j'ai à nouveau créé une nouvelle composante, pour ensuite y dessiner mon dossier. Une fois que celui-ci était créé et a la bonne place, j'ai effectué la même commande que lors de l'expérimentation. C'est-à-dire **"scinder les corps"** puis masquer les corps en trop qui dépassent dans le creux. 

* Même avec un projet presque fini, il est toujours possible de modifier facilement le modèle 3D via le **tableau excel** des **modifications des paramètres**. 

### Mon modèle 3D est enfin terminé 

![](../images/photo_23.jpg)
