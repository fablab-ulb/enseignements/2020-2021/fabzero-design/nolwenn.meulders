# Impression 3D

## Première impression 

Une fois mon objet modélisé, j'ai pu procéder à l'impression 3D. Ma chaise est déposé debout sur mon plateau d'impression, afin d'éviter les supports dans le perçage des pieds supérieurs.  
Sur prusca j'ai effectué les premiers réglages d'impressions suivantes : 
* 3 couches pour les coques horizontales et parois verticales
* Support généré automatiquement 
* Densité de remplissage : 20%
* Motif : Gyroïde
* Largeur de la bordure : 4mn

J'ai alors obtenu le résultat ci-dessous. Mes pieds supérieurs ne s'impriment pas (épaisseur du pied trop fine pour la buse  de la machine) et aucun support n'est généré en dessous de mon assise. 

![Photo 14 et 15](../images/Photo_14-15.png)

Pour résoudre ce problème j'ai dû:   
* Agandrir la chaise avec comme facteur de redimensionnement x150
* Décocher la case "ne pas supporter les ponts"

J'ai donc pu exporter le Gcode, importer le fichier sur la carte mémoire de l'imprimante 3D et lancer l'impression.  
J'ai lancé en parrallèle une impression de deux chaises (bleues) avec des pieds un peu plus petits, afin de tester l'assemblage de mes chaises, voir ce qui fonctionnerait le mieux. Lors des réglages d'impression de ces chaises bleu j'ai seulement changé le motif de remplissage gyroïdde par triangle

![résultat impression](../images/photo_14.jpg)

## Porblèmes survenus

Après l'impression 3D, j'ai pu observer une très nette différence entre ma chaise orange et mes chaises bleues.  
Les chaises bleues étaient beaucoup plus fragiles et présentaient toutes les deux des petits défauts d'impressions. Alors que j'enlevais mes supports, 5 pieds sur 8 se sont cassés. La chaise orange était quant à elle beaucoup plus solide, l'impression beaucoup plus lisse et nette. À l'heure d'aujourd'hui, je n'ai toujours pas compris la nature de ces différences (le fil d'impression d'une autre qualité? Le motif de remplissage qui rend l'ensemble plus/moins solide?). 

![Defauts](../images/photo_15.jpg)

Ensuite, j'ai remarqué un problème avec l'emboitage des pièces. Les pieds inférieurs rentraient facilement dans le perçage des pieds supérieurs (bien que le trou puisse être légèrement plus petit). Seulement, les pieds n'étant pas parfaitement alignés, il est impossible d'assembler entièrement les chaises les unes sur les autres (les deux pieds ne peuvent s'emboiter en même temps). 

![emboitage](../images/Photo_16.JPG) 

À la suite de cette première expérience, j'ai modifié mon modèle 3D afin de résoudre mes problème d'assemblage. 


## Impression après modification

Après modification de mon modèle 3D, j'ai voulu réimprimer deux chaises pour pouvoir tester leur assemblage. J'ai importé deux fois mon fichier .3MF sur prusca (ma chaise exportée de fusion). J'ai placé les deux chaises sur assez loin l'une de l'autre de façon à ce l'impression de l'une ne gène pas l'impression de l'autre. Cependant, il faut savoir qu'aux plus les objets sont éloignés sur le plateau, au plus **l'impression sera longue**. 
J'ai effectué les mêmes réglages sur prusca que lors de mon impression précédente. Cependant, mon modèle 3D n'arrivait pas à s'imprimer correctement. Dès la première couche, on pouvait observer des gros défauts d'impressions. 
 
![Photo fail](../images/Photo_26.png) 

Le souci venait en fait d'une mauvaise manipulation de ma part. J'avais sélectionné **imprimante originale mini** alors que les machines du fablab sont des **original Prusca i3 MK3S**. Il faut sélectionner la bonne imprimante dans les paramètres pour que celle-ci puisse analyser le modèle correctement. 

![Photo prusca](../images/Photo_24.jpg) 

Une fois ce souci résolu, l'impression de mes chaises s'est déroulé sans soucis. Voici le résultat final ! 

![résultat final](../images/Photo_25.jpg)


