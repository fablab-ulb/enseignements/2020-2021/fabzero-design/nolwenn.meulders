# Découpe laser

## Découpe assistée par un ordinateur

Le fablab dispose de deux découpeuses lasers. Une grande, l'OpenSource (1m22x77cm) et d'une plus petite, la muse (50x30cm).  
Nous avons tout d'abord eu un cours qui nous expliquait le fonctionnement des machines. Le récapitulatif de ce cours se trouve sur les liens ci-dessous ;
- [Slides sur la formation laser](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/LASER_cutter.pdf)
- [Fonctionnement de la lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/Manuel_Lasersaur.pdf)

L'énoncé du travail était de réaliser, grâce à la découpe laser, un **abat-jour** inspiré de notre objet. Nous avons utilisé la grande machine **OpenSource** et la matière utilisée était une **feuille de polypropylène**. 


### La création du patron
La première chose qui nous était demandée, était de créer un abat-jour qui avait pour inspiration notre objet. Dans mon cas, la chaise d'enfant. J'ai alors décidé d'inscrire les mots qui me venait en premier quand je pensais à mon objet : 
- modulable
- assemblage
- combinaison
- emboîtable  

Mon idée était de créer un luminaire en forme de "L" emboîtable, qui pourrait se combiner à l'infini avec des luminaires semblables à lui-même. J'ai d'abord dessiné mon idée, pour ensuite faire une mini maquette en papier de mon projet. J'ai créé une trame de 3x3cm (petit échelle pour le premier test), dans la quelle viendrait s'imbriquer différentes formes. Mon "L" est réalisé par l'assemblage de trois cubes de 3x3cm. 

![esquisse](../images/photo_17.jpg)

### Modélisation du patron
Une fois mon patron en papier réalisé, je l'ai modélisé sur Illustrator.  Comme il ne s'agissait que de carré, c'était très facile de retracer mon patron sur ce programme. D'autant plus que celui-ci me permettait de convertir directement mon fichier en **SVG**. En effet, la machine Open-source peut lire les fichiers en formats **SVG** et **DXS**. Il est préférable d'utiliser l'extension SVG, car l'extension DXS est à l'origine d'énormément de problèmes lors de l'impression.  Sur Illustrator j'ai utilisé deux types de couleurs afin de faciliter la découpe. Le rouge pour tous les traits de pliages (ce qui doit seulement être gravé) et le noir pour le contour du patron (ce qu'il faut découper).

### Découpe laser 
Pour utiliser la machine il est important d'avoir connaissance des **conseils d'utilisation** ainsi que des **précautions** à prendre. Ceux-ci se trouvent sur [les slides de la formation laser](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/LASER_cutter.pdf).  
Une fois mon fichier SVG importé sur l'ordinateur de la machine, le premier problème est survenu. Tout était en un "bloc" et le programme ne différenciait pas les traits rouges, destiné à la gravure, des traits noirs pour la découpe. De plus, celui-ci était beaucoup trop grand et plus du tout à l'echelle demandée.
J'ai alors ouvert mon fichier sur inscape pour dégrouper l'ensemble. Il fallait seulement l'ouvrir dans le programme, aucune manipulation supplémentaire n'était nécessaire. 
Pour mettre à l'échelle mon modèle j'ai dû aller dans images -> dimensions et indiquer la dimension initiale (soit 24cm de longueur car mon patron est une succession de 8 carrés de 3cm). 
J'ai enregistré et enfin put rouvrir mon fichier dans le programme de l'OpenSource. 
La couleur sur mon modèle permet au programme de différencier les types de traits. On règle la vitesse (**%**) et la puissance (**F**) en fonction du résultas que l'on souhaite. Au plus la découpe est puissante et lente, au plus la force de découpe sera importante.  
Dans le programme, il faut sélectionner la couleur et ainsi lui donner les paramètres souhaités. L'idéal est de découper les contours en premier (pass1), au cas où la feuille de polypropylène bougerait lors de la découpe.  
Dans mon cas j'ai utilisé comme paramètres :
- **Pour la découpe** : Une puissance de **F2600** et une vitesse de **40%**  
- **Pour la gravure** : Une puissance de **F1500** et une vitesse de **50%**

La découpe s'est arrêtée après avoir découpé les contours. J'ai donc du : Stopper l'impression -> masquer les traits noirs de découpe -> relancer l'impression. Comme ça la machine ne recommençait pas la découpe de mes traits noirs.  
Par chance, ma première expérimentation était une réussite et j'ai directement obtenu le résultat souhaité. Le seul problème que j'ai est au niveau de la création du patron. En effet, celui-ci ne me permet pas d'assembler ma forme sans l'utilisation de colle, papier collant,... La prochaine étape de mon travail sera donc de créer un patron capable de se suffire à lui-même (ou je n'aurai pas besoin de papier collant pour faire tenir ma forme). 

![Patron](../images/photo_18.jpg)

### Résultat final

![Photo abat-jour](../images/photo_19.jpg)
